#! /usr/bin/env python3
import os
from argparse import ArgumentParser

def main():
    parser = ArgumentParser(description="Make dictionary from N-Gram data")
    # Arguments
    parser.add_argument("-i", "--input-dir", help="N-Gram result directory", required=True)
    parser.add_argument("-o", "--output", help="Output dictionary path", required=True)
    parser.add_argument("-n", "--n-words", help="Maximum number of words in the dictionary", type=int, required=True)
    # Parse arguments
    args = parser.parse_args()
    # Dictionary
    dictionary = {}
    n_words = 0
    end_flag = False
    # Open files and generate dictionary
    for entry in os.scandir(args.input_dir):
        print("Opening file: %s" % entry.name)
        with open(os.path.join(args.input_dir, entry.name)) as gram_file:
            # Read lines
            for line in gram_file.readlines():
                split_line = line.split()
                if len(split_line)!=2:
                    continue
                word, suff_word = split_line
                # Word already present
                if word in dictionary:
                    continue
                # Insert word into dictionary
                print("#%d: %s %s" % (n_words, word, suff_word))
                dictionary[word] = suff_word
                n_words += 1
                # Limit reached
                if n_words>=args.n_words:
                    end_flag = True
                    break
        if end_flag:
            break
    # Write dictionary into file
    with open(args.output, "w") as output_file:
        for word, suff_word in dictionary.items():
            output_file.write("%s %s\n" % (word, suff_word))

if __name__=="__main__":
    main()
