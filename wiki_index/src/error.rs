use std::result;
use std::io::Error as IOError;
use std::error::Error as ErrorTrait;
use std::fmt;
use std::string::{ToString};

use backtrace::{Backtrace};
use serde_json::error::Error as SerdeJSONError;

#[derive(Debug)]
/// Wiki index error kinds
pub enum ErrorKind {
    /// Cannot write or modify object
    NonWritable,
    /// Cannot expand object
    NonExpandable,
    /// Not found
    NotFound,
    /// Insufficient memory
    InsufficientMemory,
    /// Other error
    Other,
    /// Invalid data
    InvalidData,
    /// Already present
    AlreadyPresent,
    /// System error
    SysError(IOError),
    /// Serde JSON error
    SerdeJSONError(SerdeJSONError)
}

#[derive(Debug)]
/// Wiki index error type
pub struct Error {
    /// Error kind
    pub kind: ErrorKind,
    /// Error description
    pub description: Option<String>,
    /// Error backtrace
    pub backtrace: Backtrace
}

/// Wiki index result type
pub type Result<T> = result::Result<T, Error>;

impl ErrorKind {
    /// Get default description for different kinds of error
    pub fn as_str(&self) -> &'static str {
        match *self {
            ErrorKind::NonWritable => "Cannot write or modify non-writable object",
            ErrorKind::NonExpandable => "Cannot expand non-expandable object",
            ErrorKind::NotFound => "Object or entity not found",
            ErrorKind::Other => "Other unknown error",
            ErrorKind::InsufficientMemory => "Insufficient memory for operation",
            ErrorKind::InvalidData => "Invalid data or type mismatch",
            ErrorKind::AlreadyPresent => "Data or object already present",
            ErrorKind::SysError(..) => "System error",
            ErrorKind::SerdeJSONError(..) => "Serde JSON error"
        }
    }
}

impl Error {
    /// Create a new error object with default description
    pub fn simple(kind: ErrorKind) -> Error {
        Error {
            kind: kind,
            description: None,
            backtrace: Backtrace::new()
        }
    }

    /// Create a new error object
    pub fn new<T: ToString>(kind: ErrorKind, description: T) -> Error {
        Error {
            kind: kind,
            description: Some(description.to_string()),
            backtrace: Backtrace::new()
        }
    }
}

impl fmt::Display for Error {
    /// Format and output error
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}", self.description())?;

        //Output cause
        match self.cause() {
            Some(error) => write!(fmt, ": {}", error),
            None => Ok(())
        }
    }
}

impl ErrorTrait for Error {
    /// Get description of the error
    fn description(&self) -> &str {
        match self.description {
            Some(ref description) => description.as_str(),
            None => self.kind.as_str()
        }
    }

    /// Get low-level cause of the error
    fn cause(&self) -> Option<&ErrorTrait> {
        match self.kind {
            ErrorKind::SysError(ref error) => Some(error),
            ErrorKind::SerdeJSONError(ref error) => Some(error),
            _ => None
        }
    }
}

impl From<ErrorKind> for Error {
    /// Convert from error type to error object
    fn from(kind: ErrorKind) -> Error { Error::simple(kind) }
}

impl From<IOError> for Error {
    /// Convert from system error to Wiki index error
    fn from(error: IOError) -> Error { Error::simple(ErrorKind::SysError(error)) }
}

impl From<SerdeJSONError> for Error {
    /// Convert from system error to Wiki index error
    fn from(error: SerdeJSONError) -> Error { Error::simple(ErrorKind::SerdeJSONError(error)) }
}
