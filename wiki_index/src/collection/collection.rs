use std::collections::{VecDeque};
use std::fs::{OpenOptions, File, create_dir};
use std::io::{Read, Write, Seek, SeekFrom};
use std::path::{PathBuf};

use serde_json;
use serde_json::{Value};

use error::{Result, Error, ErrorKind};
use mmap::{MmapOpenOptions, MmapData, MmapArray};
use block::{RcBlockManager, RcBlock, WeakBlock};
use collection::link::{CollectionLink};

/// Collection type
#[repr(u32)]
#[derive(Copy, Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub enum CollectionType {
    /// Word index
    WordIndex = 0,
    /// Word metadata
    WordMetadata = 1,
    /// Article index
    ArticleIndex = 2,
    /// Article metadata
    ArticleMetadata = 3,
    /// Matrix index
    MatrixIndex = 4,
    /// Matrix metadata
    MatrixMetadata = 5
}

/// Collection common structure
#[derive(Debug)]
pub struct CollectionBase {
    /// Open options
    pub open_options: MmapOpenOptions,
    /// Collection information
    pub info: Value,
    /// Block manager
    pub(super) block_manager: RcBlockManager,
    /// Collection information file
    info_file: File,
    /// Block look-up cache
    block_cache: VecDeque<(u64, WeakBlock)>
}

/// Block look-up cache size
static COL_BLOCK_CACHE_SIZE: usize = 4;
/// Collection information file name
static COL_INFO_FILE_NAME: &str = "info.json";

impl CollectionBase {
    /// Create a new collection base
    pub fn new(
        block_manager: RcBlockManager,
        name: String,
        collection_type: CollectionType,
        open_options: MmapOpenOptions
    ) -> Result<CollectionBase> {
        //Collection path
        let collection_path = block_manager.borrow().root_dir.join(name.clone());
        //Collection directory does not exist
        if !collection_path.exists() {
            //Create collection directory
            if open_options.create {
                create_dir(collection_path.clone())?;
            } else {
                return Err(Error::new(
                    ErrorKind::NotFound,
                    "Cannot find directory for collection!"
                ));
            }
        }

        //Build open options for collection information file
        let mut info_file_options = OpenOptions::new();
        info_file_options.read(true);
        if open_options.writable {
            info_file_options.write(true).create(open_options.create);
        }
        //Open information file
        let mut info_file = info_file_options.open(collection_path.join(COL_INFO_FILE_NAME))?;
        //Read file content
        let mut info_content = String::new();
        info_file.read_to_string(&mut info_content)?;
        //Empty flag
        let empty_info = info_content.len()==0;
        //Parse JSON for existing file, or write and flush information
        let info = if !empty_info {
            serde_json::from_str(&info_content)?
        } else {
            json!({
                "name": name,
                "collection_type": collection_type,
                "n_blocks": 1
            })
        };

        //Create collection base
        let mut collection_base = CollectionBase {
            open_options: open_options,
            block_manager: block_manager,
            info: info,
            info_file: info_file,
            block_cache: VecDeque::new()
        };

        //Flush information for newly created file
        if empty_info {
            collection_base.flush_info()?;
        }

        Ok(collection_base)
    }

    /// Flush collection information to file
    pub fn flush_info(&mut self) -> Result<()> {
        //Serialize information into string
        let info_content = serde_json::to_string(&self.info)?;
        //Restore file pointer
        self.info_file.seek(SeekFrom::Start(0))?;
        //Set file length and write data
        self.info_file.set_len(info_content.len() as u64)?;
        self.info_file.write(info_content.as_bytes())?;
        //Flush file content to disk
        self.info_file.sync_all()?;

        Ok(())
    }

    /// Get current collection block by ID (ID-partitioned collection only)
    pub fn block_by_id(&mut self, block_id: u64, pinned: bool) -> Result<RcBlock> {
        //Look up in cache first
        let search_result = self.block_cache.iter()
            .enumerate()
            .find(|&(_, &(id, _))| id==block_id)
            .map(|(index, &(_, ref weak_block))| (index, weak_block.clone()));
        //Found in cache
        match search_result {
            Some((index, ref weak_block)) => {
                //Try upgrade reference
                match weak_block.upgrade() {
                    //Move block to beginning of the cache
                    Some(rc_block) => {
                        self.block_cache.swap(0, index);

                        return Ok(rc_block);
                    },
                    //Block swapped out of memory
                    None => {
                        self.block_cache.remove(index);
                    }
                }
            },
            None => ()
        }

        //Get block path
        let block_path = {
            let collection_name = self.info.get("name")
                .ok_or(Error::simple(ErrorKind::NotFound))?
                .as_str()
                .ok_or(Error::simple(ErrorKind::InvalidData))?;
            let block_file_name = format!("{}.block", block_id);

            PathBuf::from(collection_name).join(block_file_name)
        };
        //Get block from manager
        let weak_block = self.block_manager.borrow_mut().block(
            block_path,
            self.open_options.clone(),
            pinned
        )?;

        //Update block cache
        if self.block_cache.len()>=COL_BLOCK_CACHE_SIZE {
            self.block_cache.pop_back();
        }
        self.block_cache.push_front((block_id, weak_block.clone()));

        Ok(weak_block.upgrade().unwrap())
    }

    /// Write data to collection; open a new block if necessary
    /// (ID-partitioned collection only)
    pub fn alloc_data<T>(&mut self) -> Result<(u64, MmapData<T>)> {
        //Get current block ID
        let mut current_id = self.info.get("n_blocks")
            .ok_or(Error::simple(ErrorKind::NotFound))?
            .as_u64()
            .ok_or(Error::simple(ErrorKind::InvalidData))?;
        current_id -= 1;

        //Get current block buffer
        let buffer = self.block_by_id(current_id, false)?.buffer.clone();
        //Try to write data to block
        let wrapper = match MmapData::<T>::alloc(buffer) {
            //Allocation successful; write data
            Ok(wrapper) => wrapper,
            //Allocation failed
            Err(error) => {
                //Ignore insufficient memory error
                match error.kind {
                    ErrorKind::NonExpandable => (),
                    _ => { return Err(error); }
                }

                //Update number of blocks
                current_id += 1;
                self.info["n_blocks"] = json!(current_id+1);
                //Flush information
                self.flush_info()?;

                //Get next block
                let buffer = self.block_by_id(current_id, false)?.buffer.clone();
                //Try to allocate memory again
                MmapData::<T>::alloc(buffer)?
            }
        };

        //Get block ID and wrapper
        Ok((current_id, wrapper))
    }

    /// Allocate variable-length data ; open a new block if necessary
    /// (ID-partitioned collection only)
    pub fn alloc_vary<T>(&mut self, size: u32) -> Result<(u64, MmapArray<T>)> {
        //Get current block ID
        let mut current_id = self.info.get("n_blocks")
            .ok_or(Error::simple(ErrorKind::NotFound))?
            .as_u64()
            .ok_or(Error::simple(ErrorKind::InvalidData))?;
        current_id -= 1;

        //Get current block buffer
        let buffer = self.block_by_id(current_id, false)?.buffer.clone();
        //Try to write data to block
        let wrapper = match MmapArray::<T>::alloc(buffer, size) {
            //Allocation successful; write data
            Ok(wrapper) => wrapper,
            //Allocation failed
            Err(error) => {
                //Ignore insufficient memory error
                match error.kind {
                    ErrorKind::NonExpandable => (),
                    _ => { return Err(error); }
                }

                //Update number of blocks
                current_id += 1;
                self.info["n_blocks"] = json!(current_id+1);
                //Flush information
                self.flush_info()?;

                //Get next block
                let buffer = self.block_by_id(current_id, false)?.buffer.clone();
                //Try to allocate memory again
                MmapArray::<T>::alloc(buffer, size)?
            }
        };

        //Get block ID and wrapper
        Ok((current_id, wrapper))
    }

    /// Write data to collection
    pub fn write_data<T>(&mut self, data: T) -> Result<CollectionLink<T>> {
        let (block_id, wrapper) = self.alloc_data()?;
        //Write metadata
        *wrapper.borrow_mut() = data;

        Ok(CollectionLink::new(block_id, wrapper.as_link()))
    }

    /// Write variable-length data to collection
    pub fn write_vary<T: Clone>(&mut self, data: &[T]) -> Result<CollectionLink<T>> {
        let (block_id, wrapper) = self.alloc_vary(data.len() as u32)?;
        //Write variable-length metadata
        wrapper.borrow_mut().clone_from_slice(data);

        Ok(CollectionLink::new(block_id, wrapper.as_link()))
    }
}
