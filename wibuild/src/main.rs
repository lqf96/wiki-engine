extern crate clap;
extern crate wiki_index;

use std::path::{PathBuf};
use std::fs::{OpenOptions, read_dir};
use std::io::{BufRead, BufReader};

use clap::{App, Arg};
use wiki_index::error::{Result};
use wiki_index::wiki_index::{WikipediaIndex};
use wiki_index::mmap::{MmapOpenOptions};

/// Main program
fn start() -> Result<()> {
    //CLI argument matches
    let arg_matches = App::new("Wiki index builder")
        .version("0.1.0")
        .author("Qifan Lu <lqf.1996121@gmail.com>")
        //CLI arguments
        .arg(Arg::with_name("input-dir")
            .short("-i")
            .value_name("INPUT_DIR")
            .help("Processed Wikipedia dataset directory")
            .required(true)
        )
        .arg(Arg::with_name("output-dir")
            .short("-o")
            .value_name("INDEX_DIR")
            .help("Wikipedia index directory")
            .required(true)
        )
        .get_matches();

    //Open Wikipedia index
    let index = WikipediaIndex::new(
        PathBuf::from(arg_matches.value_of("output-dir").unwrap()),
        8589934592,
        MmapOpenOptions {
            writable: true,
            expandable: true,
            create: true,
            init_size: 4096,
            max_size: 16777216
        }
    )?;
    //Sparse matrix
    let matrix = index.matrix.clone();

    //Input directory
    let input_dir = PathBuf::from(arg_matches.value_of("input-dir").unwrap());

    //Read files from directory
    let entry_iter = read_dir(input_dir.clone())?;
    for (i, entry) in entry_iter.enumerate() {
        let entry = entry?;

        let file_name = match entry.file_name().into_string() {
            Ok(name) => name,
            Err(..) => { continue; }
        };
        //Ignore non-result files
        if !file_name.ends_with(".txt") {
            continue;
        }

        //Open file
        let file = OpenOptions::new()
            .read(true)
            .open(input_dir.join(file_name))?;
        let file = BufReader::new(file);

        //Article name
        let mut article_name = String::new();
        //Get word name
        for (j, line) in file.lines().enumerate() {
            let line = line?;
            //Ignore empty line
            if line.len()==0 {
                continue;
            }

            //Article name
            if j==0 {
                article_name = line.trim_right_matches(".html").replace('_', " ");
                continue;
            }

            //Split line
            let mut split_line_iter = line.split_whitespace();
            //Get word
            let word = split_line_iter.next().unwrap().to_string();
            if j%100==0 {
                println!("Insert #{} '{}' in #{} '{}'", j, &word, i, &article_name);
            }
            //Insert article-word pair into matrix
            matrix.borrow_mut().insert(word, article_name.clone())?;
        }
    }

    Ok(())
}

/// Program entry
fn main() {
    match start() {
        Ok(..) => (),
        //Print error and backtrace
        Err(error) => {
            eprintln!("Error: {}", error);
            eprintln!("{:?}", error.backtrace);
        }
    }
}
