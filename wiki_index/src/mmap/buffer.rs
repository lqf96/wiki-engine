use std::cell::{RefCell};
use std::fs::{File, OpenOptions};
use std::path::{Path};
use std::default::{Default};
use std::rc::{Rc};

use error::{Result, Error, ErrorKind};
use memmap::{MmapOptions, Mmap, MmapMut};

/// Memory-mapped file open arguments
#[derive(Clone, Debug)]
pub struct MmapOpenOptions {
    /// Writable
    pub writable: bool,
    /// Expandable
    pub expandable: bool,
    /// Create if file does not exist
    pub create: bool,
    /// Initial file size for newly created file
    pub init_size: u32,
    /// Maximum file size
    pub max_size: u32
}

/// Memory-mapped buffer wrapper
#[derive(Debug)]
enum MmapWrapper {
    /// Nothing
    None,
    /// Immutable memory map
    Immutable(Mmap),
    /// Mutable memory map
    Mutable(MmapMut)
}

/// Borrow state
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub(super) enum BorrowState {
    /// No borrow
    NoBorrow,
    /// Immutable borrow
    Borrow(u32),
    /// Mutable borrow
    BorrowMut
}

/// Memory-mapped buffer
#[derive(Debug)]
pub struct MmapBuffer {
    /// Open options
    pub options: MmapOpenOptions,
    /// Size
    pub size: u32,
    /// Cursor position
    pub(super) pos: u32,
    /// Borrow state
    pub(super) borrow_state: RefCell<BorrowState>,
    /// Backing file
    file: File,
    /// Memory-mapped buffer
    buffer: MmapWrapper
}

/// Reference-counted memory-mapped buffer type
pub type RcMmapBuffer = Rc<RefCell<MmapBuffer>>;

/// Default initial memory-mapped file size
pub static MMAP_FILE_INIT_SIZE: u32 = 4096;
/// Default maximum memory-mapped file size
pub static MMAP_FILE_MAX_SIZE: u32 = 33554432;

/// No memory currently mapped
static PROMPT_NO_MEM_MAPPED: &str = "No memory currently mapped";
/// Attepmt to borrow mutably when immutable borrow exists
static PROMPT_REF_EXIST: &str = "Attepmt to borrow mutably when immutable borrow exists";
/// Attepmt to borrow immutably when mutable borrow exists
static PROMPT_MUT_EXIST: &str = "Attepmt to borrow immutably when mutable borrow exists";
/// Attepmt to borrow mutably twice
static PROMPT_MULTI_MUT: &str = "Attepmt to borrow mutably twice";

impl Default for MmapOpenOptions {
    /// Get default memory-mapped file configuration
    fn default() -> Self {
        MmapOpenOptions {
            writable: false,
            expandable: false,
            create: false,
            init_size: MMAP_FILE_INIT_SIZE,
            max_size: MMAP_FILE_MAX_SIZE
        }
    }
}

impl MmapBuffer {
    /// Open an memory-mapped file
    pub fn open<P: AsRef<Path>>(path: P, options: MmapOpenOptions) -> Result<Self> {
        //Build open options
        let mut open_options = OpenOptions::new();
        open_options.read(true);
        if options.writable {
            open_options.write(true).create(options.create);
        }
        //Open file
        let file = open_options.open(path)?;

        //Get file metadata
        let metadata = file.metadata()?;
        //Expand new file to initial size or get file size from metadata
        let mut size = metadata.len() as u32;
        if size<MMAP_FILE_INIT_SIZE {
            file.set_len(options.init_size as u64)?;
            size = options.init_size;
        };

        //Map file to memory
        let mmap_options = MmapOptions::new();
        let buffer = unsafe {
            if options.writable {
                MmapWrapper::Mutable(mmap_options.map_mut(&file)?)
            } else {
                MmapWrapper::Immutable(mmap_options.map(&file)?)
            }
        };

        Ok(MmapBuffer {
            options: options,
            size: size,
            file: file,
            buffer: buffer,
            pos: 0,
            borrow_state: RefCell::new(BorrowState::NoBorrow)
        })
    }

    /// Expand file and remap file into memory
    pub fn expand(&mut self, expand_size: Option<u32>) -> Result<u32> {
        //Buffer must be writable and expandable
        if !(self.options.writable&&self.options.expandable) {
            return Err(Error::simple(ErrorKind::NonExpandable));
        }

        //Calculate new buffer size
        //(Expand size defaults to current buffer size)
        let new_size = self.size+expand_size.unwrap_or(self.size);
        //Buffer would exceed maximum size limit
        if new_size>self.options.max_size {
            return Err(Error::new(
                ErrorKind::NonExpandable,
                "Buffer would exceed maximum size limit"
            ));
        }

        //Unmap old buffer
        self.buffer = MmapWrapper::None;
        //Expand backing file
        self.file.set_len(new_size as u64)?;
        //Map file to memory
        let mmap_options = MmapOptions::new();
        self.buffer = unsafe {
            if self.options.writable {
                MmapWrapper::Mutable(mmap_options.map_mut(&self.file)?)
            } else {
                MmapWrapper::Immutable(mmap_options.map(&self.file)?)
            }
        };
        //Set new size
        self.size = new_size;

        Ok(new_size)
    }

    /// Get pointer from offset
    pub(super) fn pointer<T>(&self, offset: u32) -> *const T {
        let base_ptr = match self.buffer {
            MmapWrapper::Immutable(ref buf) => buf.as_ptr(),
            MmapWrapper::Mutable(ref buf) => buf.as_ptr(),
            MmapWrapper::None => panic!(PROMPT_NO_MEM_MAPPED)
        };

        base_ptr.wrapping_offset(offset as isize) as *const T
    }

    /// Get mutable pointer from offset
    pub(super) fn pointer_mut<T>(&mut self, offset: u32) -> *mut T {
        let base_ptr_mut = match self.buffer {
            MmapWrapper::Immutable(..) => panic!("Cannot get mutable pointer for immutable mapping!"),
            MmapWrapper::Mutable(ref mut buf) => buf.as_mut_ptr(),
            MmapWrapper::None => panic!(PROMPT_NO_MEM_MAPPED)
        };

        base_ptr_mut.wrapping_offset(offset as isize) as *mut T
    }

    /// Allocate memory from buffer and advance cursor
    pub(super) fn alloc_mem(&mut self, size: u32) -> Result<u32> {
        //Save cursor
        let cursor = self.pos;

        //Memory is insufficient
        if cursor+size>self.size {
            if self.options.writable && self.options.expandable {
                self.expand(None)?;
            } else {
                return Err(Error::simple(ErrorKind::InsufficientMemory));
            }
        }
        //Advance cursor
        self.pos = cursor+size;

        Ok(cursor)
    }

    /// Add an immutable borrow to borrow state
    pub(super) fn add_borrow(&self) {
        let mut borrow_state = self.borrow_state.borrow_mut();

        *borrow_state = match *borrow_state {
            BorrowState::NoBorrow => BorrowState::Borrow(1),
            BorrowState::Borrow(count) => BorrowState::Borrow(count+1),
            BorrowState::BorrowMut => panic!(PROMPT_MUT_EXIST)
        };
    }

    /// Add an mutable borrow to borrow state
    pub(super) fn add_mut_borrow(&self) {
        let mut borrow_state = self.borrow_state.borrow_mut();

        *borrow_state = match *borrow_state {
            BorrowState::NoBorrow => BorrowState::BorrowMut,
            BorrowState::Borrow(..) => panic!(PROMPT_REF_EXIST),
            BorrowState::BorrowMut => panic!(PROMPT_MULTI_MUT)
        };
    }

    /// Remove an immutable borrow to borrow state
    pub(super) fn remove_borrow(&self) {
        let mut borrow_state = self.borrow_state.borrow_mut();

        *borrow_state = match *borrow_state {
            BorrowState::Borrow(count) => if count==1 {
                BorrowState::NoBorrow
            } else {
                BorrowState::Borrow(count-1)
            },
            _ => unreachable!()
        };
    }

    /// Remove an mutable borrow to borrow state
    pub(super) fn remove_mut_borrow(&self) {
        let mut borrow_state = self.borrow_state.borrow_mut();

        *borrow_state = match *borrow_state {
            BorrowState::BorrowMut => BorrowState::NoBorrow,
            _ => unreachable!()
        };
    }
}
