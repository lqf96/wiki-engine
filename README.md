# Wiki Engine Project
Wiki Engine is a simple search engine for offline Wikipedia.

See [project report](REPORT.md) for structure, design and usage of the project.

## License
[MIT License](LICENSE)
