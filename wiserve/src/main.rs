#[macro_use]
extern crate serde_json;
extern crate clap;
extern crate iron;
extern crate mount;
extern crate staticfile;
extern crate params;
extern crate wiki_index;

use std::path::{PathBuf};
use std::collections::{HashSet};
use std::error::{Error};
use std::sync::{Arc, Mutex};

use clap::{App, Arg};
use iron::prelude::{Iron, Request, Response, IronResult};
use iron::{Plugin};
use iron::status;
use mount::{Mount};
use staticfile::{Static};
use params::{Params, Value};
use wiki_index::error::{Result};
use wiki_index::hash_index::{RcStringMap, RcSparseMatrix};
use wiki_index::wiki_index::{WikipediaIndex};
use wiki_index::mmap::{MmapOpenOptions};

/// Search API implementation
fn search_word(
    word_header: RcStringMap,
    article_header: RcStringMap,
    matrix: RcSparseMatrix,
    query: String
) -> Result<HashSet<String>> {
    //Search result
    let mut search_result = HashSet::new();
    //First word
    let mut first_word = true;

    for word in query.split_whitespace() {
        //Word
        let word = String::from(word);
        //Word search result
        let mut word_search_result = HashSet::new();

        //Get word entry
        let word_entry_link = word_header.borrow_mut().get(&word.clone())?;
        let word_entry = word_entry_link.try_get_data(&mut word_header.borrow_mut().base)?;
        //Get matrix item link
        let mut item_link = word_entry.borrow().list_head.clone();

        while !item_link.is_empty() {
            //Get item
            let item = item_link.try_get_data(&mut matrix.borrow_mut().base)?;
            //Get source article
            let article_link = item.borrow().article.clone();;
            let article_item = article_link.try_get_data(&mut article_header.borrow_mut().base)?;

            //Get article title
            let article_key_link = article_item.borrow().key.clone();
            let article_key_array = article_key_link.try_get_array(
                &mut article_header.borrow_mut().base
            )?;
            let article_key = unsafe {
                String::from_utf8_unchecked(Vec::from(&*article_key_array.borrow()))
            };

            word_search_result.insert(article_key);

            //Move on to next item
            item_link = item.borrow().next_word.clone();
        }

        //Intersect with search result
        search_result = if first_word {
            word_search_result
        } else {
            first_word = false;

            word_search_result.union(&search_result)
                .map(|result| result.clone())
                .collect()
        };
    }

    Ok(search_result)
}

/// Program entry
fn start() -> Result<()> {
    //CLI argument matches
    let arg_matches = App::new("Wiki index backend server")
        .version("0.1.0")
        .author("Qifan Lu <lqf.1996121@gmail.com>")
        //CLI arguments
        .arg(Arg::with_name("input-dir")
            .short("-i")
            .value_name("INDEX_DIR")
            .help("Wikipedia index directory")
            .required(true)
        )
        .arg(Arg::with_name("static-dir")
            .short("-s")
            .value_name("STATIC_DIR")
            .help("Static files directory")
            .required(true))
        .arg(Arg::with_name("host")
            .short("-h")
            .value_name("HOST")
            .help("IP address to run server")
        )
        .arg(Arg::with_name("port")
            .short("-p")
            .value_name("PORT")
            .help("Port to run server")
        )
        .get_matches();

    //Open Wikipedia index
    let index = Arc::new(Mutex::new(WikipediaIndex::new(
        PathBuf::from(arg_matches.value_of("input-dir").unwrap()),
        8589934592,
        MmapOpenOptions {
            writable: false,
            expandable: false,
            create: false,
            init_size: 4096,
            max_size: 16777216
        }
    )?));

    let mut mount = Mount::new();
    //Static files
    let static_dir = arg_matches.value_of("static-dir").unwrap();
    mount.mount("/", Static::new(PathBuf::from(static_dir)));
    //Search API
    mount.mount("/api/search", move |req: &mut Request| -> IronResult<Response> {
        let index = index.lock().unwrap();

        let word_header = index.word_header.clone();
        let article_header = index.article_header.clone();
        let matrix = index.matrix.clone();

        let params = req.get_ref::<Params>().unwrap();
        let query = match params.get("q") {
            Some(&Value::String(ref query)) => query.clone(),
            _ => { return Ok(Response::with(status::NotFound)) }
        };

        let resp = match search_word(
            word_header.clone(),
            article_header.clone(),
            matrix.clone(),
            query
        ) {
            //Successfully completed
            Ok(result) => {
                let response_str = serde_json::to_string(&json!({
                    "status": "success",
                    "result": result
                })).unwrap();

                (status::Ok, response_str)
            }
            //Error happened
            Err(error) => {
                let response_str = serde_json::to_string(&json!({
                    "status": "failed",
                    "description": error.description()
                })).unwrap();

                (status::BadRequest, response_str)
            }
        };

        Ok(Response::with(resp))
    });

    //Host and port
    let host_str = arg_matches.value_of("host").unwrap_or("127.0.0.1");
    let port_str = arg_matches.value_of("port").unwrap_or("8080");
    //Run server
    Iron::new(mount).http(format!("{}:{}", host_str, port_str)).unwrap();

    Ok(())
}

/// Program entry
fn main() {
    match start() {
        Ok(..) => (),
        //Print error and backtrace
        Err(error) => {
            eprintln!("Error: {}", error);
            eprintln!("{:?}", error.backtrace);
        }
    }
}
