use std::rc::{Rc};
use std::cell::{RefCell};
use std::path::{PathBuf};
use std::fs::{create_dir};
use std::marker::{Send};

use error::{Result};
use mmap::{MmapOpenOptions};
use collection::{CollectionType};
use block::{BlockManager, RcBlockManager};
use hash_index::{StringMap, RcStringMap, SparseMatrix, RcSparseMatrix};

/// Wikipedia index
pub struct WikipediaIndex {
    /// Block manager
    pub block_manager: RcBlockManager,
    /// Word header
    pub word_header: RcStringMap,
    /// Article header
    pub article_header: RcStringMap,
    /// Sparse matrix
    pub matrix: RcSparseMatrix
}

impl WikipediaIndex {
    /// Create a new Wikipedia index
    pub fn new(
        root_dir: PathBuf,
        max_size: u64,
        open_options:
        MmapOpenOptions
    ) -> Result<WikipediaIndex> {
        //Create root directory if it does not exist
        if !root_dir.exists() {
            create_dir(root_dir.clone())?;
        }
        //Block manager
        let block_manager = Rc::new(RefCell::new(
            BlockManager::new(root_dir.clone(), max_size)
        ));

        //Word header
        let word_header = Rc::new(RefCell::new(StringMap::new(
            block_manager.clone(),
            "word_header".to_owned(),
            CollectionType::WordIndex,
            open_options.clone()
        )?));
        //Article header
        let article_header = Rc::new(RefCell::new(StringMap::new(
            block_manager.clone(),
            "article_header".to_owned(),
            CollectionType::ArticleIndex,
            open_options.clone()
        )?));

        //Word-article sparse matrix
        let matrix = Rc::new(RefCell::new(SparseMatrix::new(
            block_manager.clone(),
            "matrix".to_owned(),
            open_options.clone(),
            word_header.clone(),
            article_header.clone()
        )?));

        Ok(WikipediaIndex {
            block_manager: block_manager,
            word_header: word_header,
            article_header: article_header,
            matrix: matrix
        })
    }
}

unsafe impl Send for WikipediaIndex {}
