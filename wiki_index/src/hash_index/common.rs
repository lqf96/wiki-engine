use collection::{CollectionLink};

/// Metadata information linked list
#[derive(Clone, Debug)]
pub(super) struct MetadataInfo {
    /// Metadata collection name
    name: CollectionLink<u8>,
    /// Variable-length data
    variable: bool,
    /// Next linked list item
    next: CollectionLink<MetadataInfo>
}

/// Metadata data linked list
#[derive(Clone, Debug)]
pub(super) struct MetadataItem {
    /// Link to metadata
    link: CollectionLink<()>,
    /// Next linked list item
    next: CollectionLink<MetadataItem>
}
