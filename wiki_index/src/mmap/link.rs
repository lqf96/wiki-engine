use std::marker::{PhantomData};

use mmap::buffer::{RcMmapBuffer};
use mmap::data::{MmapData};
use mmap::array::{MmapArray};

/// Pointer to data or array
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct MmapLink<T> {
    /// Offset
    offset: u32,
    /// Type marker
    marker: PhantomData<T>
}

impl<T> MmapData<T> {
    /// Get pointer to current data
    pub fn as_link(&self) -> MmapLink<T> {
        MmapLink {
            offset: self.offset,
            marker: PhantomData
        }
    }
}

impl<T> MmapArray<T> {
    /// Get pointer to current array
    pub fn as_link(&self) -> MmapLink<T> {
        MmapLink {
            offset: self.offset,
            marker: PhantomData
        }
    }
}

impl<T> MmapLink<T> {
    /// Create a null pointer
    pub unsafe fn empty() -> Self {
        MmapLink {
            offset: 0,
            marker: PhantomData
        }
    }

    /// Get data referenced by current pointer
    pub fn get_data(&self, buffer: RcMmapBuffer) -> MmapData<T> {
        unsafe { MmapData::new(buffer, self.offset) }
    }

    /// Get array referenced by current pointer
    pub fn get_array(&self, buffer: RcMmapBuffer) -> MmapArray<T> {
        unsafe { MmapArray::new(buffer, self.offset) }
    }

    /// Is current pointer empty
    pub fn is_empty(&self) -> bool {
        self.offset==0
    }
}
