use error::{Result};
use collection::{CollectionType, CollectionBase, CollectionLink};
use block::{RcBlockManager};
use mmap::{MmapOpenOptions};

/// Metadata collection
pub struct MetadataCollection {
    /// Collection base
    pub base: CollectionBase
}

impl MetadataCollection {
    /// Create a new metadata collection
    pub fn new(
        block_manager: RcBlockManager,
        name: String,
        collection_type: CollectionType,
        open_options: MmapOpenOptions
    ) -> Result<MetadataCollection> {
        Ok(MetadataCollection {
            base: CollectionBase::new(block_manager, name, collection_type, open_options)?
        })
    }

    /// Read metadata from collection
    pub fn read_data<T: Clone>(&mut self, link: CollectionLink<T>) -> Result<T> {
        let wrapper = link.try_get_data(&mut self.base)?;
        let data = wrapper.borrow().clone();

        Ok(data)
    }

    /// Read variable-length metadata to collection
    pub fn read_vary<T: Clone>(&mut self, link: CollectionLink<T>) -> Result<Vec<T>> {
        let wrapper = link.try_get_array(&mut self.base)?;
        let data = wrapper.borrow().to_vec();

        Ok(data)
    }

    /// Write data to collection
    pub fn write_data<T: Clone>(&mut self, data: T) -> Result<CollectionLink<T>> {
        self.base.write_data(data)
    }

    /// Write variable-length data to collection
    pub fn write_vary<T: Clone>(&mut self, data: &[T]) -> Result<CollectionLink<T>> {
        self.base.write_vary(data)
    }
}
