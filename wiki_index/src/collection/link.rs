use error::{Result};
use mmap::{MmapData, MmapArray, MmapLink};
use collection::collection::{CollectionBase};

/// ID-based cross-block pointer to memory-mapped data or array
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct CollectionLink<T> {
    /// Block ID
    block_id: u64,
    /// Link for the block
    link: MmapLink<T>
}

impl<T> CollectionLink<T> {
    /// Create a new cross-block data link
    pub fn new(block_id: u64, link: MmapLink<T>) -> Self {
        CollectionLink {
            block_id: block_id,
            link: link
        }
    }

    /// Create a null link
    pub fn empty() -> Self {
        CollectionLink::new(0, unsafe { MmapLink::empty() })
    }

    /// Try to get data referenced by current pointer
    pub fn try_get_data(&self, base: &mut CollectionBase) -> Result<MmapData<T>> {
        //Reference to buffer
        let buffer = base.block_by_id(self.block_id, false)?.buffer.clone();

        Ok(self.link.get_data(buffer))
    }

    /// Try to get array referenced by current pointer
    pub fn try_get_array(&self, base: &mut CollectionBase) -> Result<MmapArray<T>> {
        //Reference to buffer
        let buffer = base.block_by_id(self.block_id, false)?.buffer.clone();

        Ok(self.link.get_array(buffer))
    }

    /// Is current pointer empty
    pub fn is_empty(&self) -> bool {
        self.link.is_empty()
    }
}
