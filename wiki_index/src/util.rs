//All one mask
static ALL_ONE_MASK: u64 = 0xffffffffffffffff;

/// Get bits of the data
pub fn get_bits(value: u64, begin: u8, end: u8) -> u64 {
    //Data mask
    let mask = (ALL_ONE_MASK>>begin)^(ALL_ONE_MASK>>end);

    (value&mask)>>(64-end)
}

/// Get most significant bit index
pub fn msb_index(mut value: u64) -> u8 {
    let mut index = 0;

    while value!=0 {
        index += 1;
        value >>= 1;
    }

    index
}
