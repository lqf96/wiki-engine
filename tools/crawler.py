#! /usr/bin/env python3
import requests
import sys
import argparse
from bs4 import BeautifulSoup
import os

parser = argparse.ArgumentParser(description="Wikipedia article crawler")
parser.add_argument("-a", "--article", help="Article to begin crawling", required=True)
parser.add_argument("-o", "--output-dir", help="Directory to save crawled articles", required=True)
parser.add_argument("-n", "--max-length", type=int, help="Maximum amount of articles", default=5)
parser.add_argument("-p", "--url-prefix", help="Wikipedia article URL prefix", default='https://en.wikipedia.org/wiki/')

args = parser.parse_args()
os.makedirs(args.output_dir, exist_ok=True)

FORBIDDEN_PREFIXES = [
	"Main_Page",
	"Portal:",
	"Special:",
	"Talk:",
	"Wikipedia:"
]

def output_file(html, filename):
	with open(os.path.join(args.output_dir, filename+".html"), 'w') as fout:
		fout.write(html.prettify())

def get_name(url):
	return url.split("/")[-1]

def get_html(url):
	if url.startswith('/'):
		url = 'https://en.wikipedia.org'+url
	r = requests.get(url)
	return BeautifulSoup(r.text, 'html.parser')

def get_url(html):
	urls = []
	for link in html.find_all('a'):
		href = link.get('href')
		if href and (href.startswith(args.url_prefix) or href.startswith('/wiki/')) and not href[-4] == '.' and ':' not in href:
			urls.append(href)
	return urls

def crawl():
	# Get filename and HTML
	filename = pending_articles.pop()
	html = get_html(args.url_prefix+filename)
	# Write file to disk
	output_file(html, filename)
	crawled_articles.add(filename)
	print(filename)
	# Get all occured article names
	urls = get_url(html)
	for url in urls:
		article_name = get_name(url)
		# Ignore specific prefixes
		ignore_flag = False
		for prefix in FORBIDDEN_PREFIXES:
			if article_name.startswith(prefix):
				ignore_flag = True
				break
		if ignore_flag:
			continue
		# Add article to pending queue
		if article_name not in crawled_articles:
			pending_articles.add(article_name)

if __name__ == "__main__":
	init_article = args.article.replace(" ", "_")
	# Crawled articles and pending articles
	crawled_articles = set()
	pending_articles = set([init_article])
	# Begin crawling
	while pending_articles and len(crawled_articles)<args.max_length:
		crawl()
