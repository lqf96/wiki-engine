use std::rc::{Rc};
use std::cell::{RefCell};

use error::{Result};
use collection::{CollectionBase, CollectionType, CollectionLink};
use hash_index::string_map::{RcStringMap, MapEntry};
use block::{RcBlockManager};
use mmap::{MmapOpenOptions};

/// Sparse word-article matrix item
#[derive(Clone, Debug)]
pub struct MatrixItem {
    /// Word
    pub word: CollectionLink<MapEntry>,
    /// Article
    pub article: CollectionLink<MapEntry>,
    /// Next item by word linked list
    pub next_word: CollectionLink<MatrixItem>,
    /// Next item by article linked list
    pub next_article: CollectionLink<MatrixItem>,
}

/// Sparse word-article matrix
pub struct SparseMatrix {
    /// Collection base
    pub base: CollectionBase,
    /// Word header
    word_header: RcStringMap,
    /// Article header
    article_header: RcStringMap,
    /// Previous article name
    prev_article_name: String,
    /// Previous article link
    prev_article_link: CollectionLink<MapEntry>
}

/// Reference-counted sparse matrix type
pub type RcSparseMatrix = Rc<RefCell<SparseMatrix>>;

impl MatrixItem {
    /// Create a new matrix item
    fn new(word: CollectionLink<MapEntry>, article: CollectionLink<MapEntry>) -> MatrixItem {
        MatrixItem {
            word: word,
            article: article,
            next_word: CollectionLink::empty(),
            next_article: CollectionLink::empty()
        }
    }
}

impl SparseMatrix {
    /// Create a new sparse matrix
    pub fn new(
        block_manager: RcBlockManager,
        name: String,
        open_options: MmapOpenOptions,
        word_header: RcStringMap,
        article_header: RcStringMap
    ) -> Result<SparseMatrix> {
        //Collection base
        let base = CollectionBase::new(
            block_manager,
            name,
            CollectionType::MatrixIndex,
            open_options
        )?;

        Ok(SparseMatrix {
            word_header: word_header,
            article_header: article_header,
            base: base,
            prev_article_name: String::new(),
            prev_article_link: CollectionLink::empty()
        })
    }

    /// Inserd word-article pair into matrix
    pub fn insert(&mut self, word: String, article: String) -> Result<CollectionLink<MatrixItem>> {
        //Article map
        let mut article_header_mut = self.article_header.borrow_mut();
        let mut word_header_mut = self.word_header.borrow_mut();

        //Cache article link because it's usually same
        if article!=self.prev_article_name {
            self.prev_article_link = article_header_mut.insert(article)?;
        }
        //Insert and/or get article entry
        let article_link = self.prev_article_link.clone();
        let article_wrapper = article_link.try_get_data(&mut article_header_mut.base)?;
        let article_hash = article_wrapper.borrow().hash;
        //Insert and/or get word entry
        let word_link = word_header_mut.insert(word)?;
        let word_wrapper = word_link.try_get_data(&mut word_header_mut.base)?;
        let word_hash = word_wrapper.borrow().hash;

        //Allocate space for matrix item
        let (block_id, matrix_item) = self.base.alloc_data()?;
        *matrix_item.borrow_mut() = MatrixItem::new(word_link, article_link);
        //Create link
        let matrix_item_link = CollectionLink::new(block_id, matrix_item.as_link());

        //Insert into article linked list
        let mut before_link = article_wrapper.borrow().list_head.clone();
        if before_link.is_empty() {
            article_wrapper.borrow_mut().list_head = matrix_item_link.clone();
        } else {
            loop {
                //Before item
                let before_item = before_link.try_get_data(&mut self.base)?;
                //Next item
                let next_link = before_item.borrow().next_article.clone();

                //End of linked list
                if !next_link.is_empty() {
                    let next_item = next_link.try_get_data(&mut self.base)?;
                    //Get word entry in order to compare hash
                    let cmp_word_link = next_item.borrow().word.clone();
                    let cmp_word = cmp_word_link.try_get_data(&mut word_header_mut.base)?;

                    //Not insertion position yet
                    if cmp_word.borrow().hash>word_hash {
                        before_link = next_link;

                        continue;
                    }
                }

                //Update before item
                before_item.borrow_mut().next_article = matrix_item_link.clone();
                //Update current item
                matrix_item.borrow_mut().next_article = next_link;

                break;
            }
        }

        //Insert into word linked list
        let mut before_link = word_wrapper.borrow().list_head.clone();
        if word_wrapper.borrow().list_head.is_empty() {
            word_wrapper.borrow_mut().list_head = matrix_item_link.clone();
        } else {
            loop {
                //Before item
                let before_item = before_link.try_get_data(&mut self.base)?;
                //Next item
                let next_link = before_item.borrow().next_word.clone();

                //End of linked list
                if !next_link.is_empty() {
                    let next_item = next_link.try_get_data(&mut self.base)?;
                    //Get article entry in order to compare hash
                    let cmp_article_link = next_item.borrow().article.clone();
                    let cmp_article = cmp_article_link.try_get_data(&mut article_header_mut.base)?;

                    //Not insertion position yet
                    if cmp_article.borrow().hash<=article_hash {
                        before_link = next_link;

                        continue;
                    }
                }

                //Update before item
                before_item.borrow_mut().next_word = matrix_item_link.clone();
                //Update current item
                matrix_item.borrow_mut().next_word = next_link;

                break;
            }
        }

        Ok(matrix_item_link)
    }
}
