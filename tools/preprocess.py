#! /usr/bin/env python3
import os, sys, time, re
from sys import argv
from argparse import ArgumentParser
from bs4 import BeautifulSoup
from multiprocessing import Pool

STOP_WORDS = None

MIN_LENGTH = 3
MAX_LENGTH = 15

FREQ_THRES_FILE = 4
FREQ_THRES_FILE_SM = 2
FREQ_THRES_ALL = 1

def parseHTMLFile(path_in, path_out, path_gram_out, filename):
    with open(path_in, 'r', encoding='utf-8') as fin:
        text = fin.read()
    #find content part
    soup = BeautifulSoup(text, 'html.parser')
    content = soup.find(id="content")
    res = {}
    pre_word = None
    post_word = {}
    after = {}

    for word in re.split("[^a-zA-Z]+", soup.get_text(separator=' ')):
        if not word:
            continue
        word_good = word.strip().lower()
        if word_good in STOP_WORDS:
            continue
        if word_good.endswith('ies'):
            word_good = word_good[:-3]+'y'
        elif word_good.endswith('es'):
            word_good = word_good[:-2]
        elif word_good.endswith('s'):
            word_good = word_good[:-1]
        elif word_good.endswith('ied'):
            word_good = word_good[:-3]+'y'
        elif word_good.endswith('ed'):
            word_good = word_good[:-2]
        if len(word_good) < MIN_LENGTH or len(word_good) > MAX_LENGTH:
            continue
        if pre_word is not None:
            if after.get(pre_word, None) is None:
                after[pre_word] = {word_good: 1}
            else:
                if after[pre_word].get(word_good, None) is None:
                    after[pre_word][word_good] = 1
                else:
                    after[pre_word][word_good] += 1
        pre_word = word_good
        cnt = res.get(word_good, 0)
        res[word_good] = cnt+1

    for word in res:
        if word in after:
            after_word = max(after[word], key=after[word].get)
            if after[word][after_word] > FREQ_THRES_FILE or (len(after[word]) == 1 and after[word][after_word] > FREQ_THRES_FILE_SM):
                post_word[word] = after_word

    with open(path_out, 'w') as fout:
        fout.write(filename+'\n')
        for word in res:
            fout.write(word+' '+str(res[word])+'\n')

    with open(path_gram_out, 'w') as fout:
        for word in post_word:
            fout.write(word+' '+post_word[word]+'\n')

    return post_word

def parseSingleFile(files):
    post_word = {}
    with open(files, 'r') as fin:
        files = fin.readlines()
    for i, file in enumerate(files):
        if i % 100 == 0:
            print("[parseSingleFile] %d processed" % i)
        file_s = file.strip()
        res = parseHTMLFile(dataset_path+file_s,
                            result_path+file_s[:-4]+'txt',
                            gram_path+file_s[:-4]+'txt',
                            file_s)
        for word in res:
            if word in post_word:
                if post_word[word].get(res[word], None) is None:
                    post_word[word][res[word]] = 1
                else:
                    post_word[word][res[word]] += 1
            else:
                post_word[word] = {res[word]: 1}

    post_words = {}
    # for word in post_word:
    #     after_word = max(post_word[word], key=post_word[word].get)
    #     if len(post_word[word]) == 1 or post_word[word][after_word] > FREQ_THRES_ALL:
    #         post_words[word] = [after_word, post_word[word][after_word]]
    return post_words

### File name lists, corresponding to number of threads
# files = ['filename1.txt',
#          'filename2.txt',
#          'filename3.txt',
#          'filename4.txt']

def src_relative_path(relative_path):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), relative_path)

def main():
    parser = ArgumentParser()
    # Arguments
    parser.add_argument(
        "-i", "--dataset-path",
        help="Input dataset path",
        required=True
    )
    parser.add_argument(
        "-o", "--result-path",
        help="Output result path",
        required=True
    )
    parser.add_argument(
        "-g", "--gram-path",
        help="Output gram data path",
        required=True
    )
    parser.add_argument(
        "-f", "--files",
        help="File name lists",
        nargs="+",
        required=True
    )
    parser.add_argument(
        "-s", "--stop-words",
        help="Stop words list",
        default=src_relative_path("stop_words.txt")
    )
    # Parse arguments
    args = parser.parse_args()
    # Path fixes
    if not args.dataset_path.endswith("/"):
        args.dataset_path += "/"
    if not args.result_path.endswith("/"):
        args.result_path += "/"
    if not args.gram_path.endswith("/"):
        args.gram_path += "/"
    # Set global variables
    globals().update(vars(args))
    # Stop words
    global STOP_WORDS
    with open(args.stop_words, "r") as fin:
        STOP_WORDS = [w.strip() for w in fin.readlines()]
    # Create directories
    os.makedirs(args.result_path, exist_ok=True)
    os.makedirs(args.gram_path, exist_ok=True)
    # Start processing
    t = time.time()
    pool = Pool(8)
    pool_words = pool.map(parseSingleFile, args.files)
    print("[main] Time used: %fs" % (time.time()-t))

if __name__=="__main__":
    main()
