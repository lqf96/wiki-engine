use std::path::{PathBuf};
use std::rc::{Rc, Weak};
use std::cell::{RefCell};
use std::collections::{HashMap, VecDeque};

use error::{Result};
use mmap::{MmapOpenOptions, MmapBuffer, RcMmapBuffer};

/// Memory block
#[derive(Debug)]
pub struct Block {
    /// Path
    pub path: PathBuf,
    /// Memory-mapped buffer
    pub buffer: RcMmapBuffer,
    /// Pinned block
    pub pinned: bool
}

/// Reference-counted mutable memory block
pub type RcBlock = Rc<Block>;
/// Weak reference-counted mutable memory block
pub type WeakBlock = Weak<Block>;

/// Memory block manager
#[derive(Debug)]
pub struct BlockManager {
    /// Root directory
    pub root_dir: PathBuf,
    /// Maximum memory consumption
    pub max_size: u64,
    /// Current memory consumption
    pub current_size: u64,
    /// Path to block mapping
    blocks_map: HashMap<PathBuf, RcBlock>,
    /// LRU queue
    lru_queue: VecDeque<RcBlock>
}

/// Reference-counted block manager
pub type RcBlockManager = Rc<RefCell<BlockManager>>;

/// Get block memory consumption
fn block_size(buffer: &MmapBuffer) -> u64 {
    let block_size = if buffer.options.writable && buffer.options.expandable {
        buffer.options.max_size
    } else {
        buffer.size
    };

    block_size as u64
}

impl BlockManager {
    /// Create a new block manager
    pub fn new(root_dir: PathBuf, max_size: u64) -> Self {
        BlockManager {
            root_dir: root_dir,
            max_size: max_size,
            current_size: 0,
            blocks_map: HashMap::new(),
            lru_queue: VecDeque::new()
        }
    }

    /// Add block
    fn add_block(&mut self, path: PathBuf, buffer: MmapBuffer, pinned: bool) -> RcBlock {
        //Block size
        let block_size = block_size(&buffer);
        //Create a new block
        let block = Rc::new(Block {
            path: path.clone(),
            buffer: Rc::new(RefCell::new(buffer)),
            pinned: pinned
        });

        //Add block to mapping
        self.blocks_map.insert(path, block.clone());
        //Add block to LRU queue
        if !pinned {
            self.lru_queue.push_front(block.clone());
        }
        //Update size
        self.current_size += block_size;

        block
    }

    /// Remove LRU block
    fn remove_lru_block(&mut self) -> bool {
        //Pop LRU end block
        let lru_end = match self.lru_queue.pop_back() {
            Some(block) => block,
            //No block to pop
            None => { return false; }
        };

        //Remove block from mapping
        self.blocks_map.remove(&lru_end.path);
        //Update size
        self.current_size -= block_size(&lru_end.buffer.borrow());

        return true;
    }

    /// Refresh block in the LRU queue
    fn refresh_block(&mut self, block: &RcBlock) {
        let block_index = self.lru_queue.iter().position(|ref b| Rc::ptr_eq(b, block));

        match block_index {
            Some(index) => self.lru_queue.swap(0, index),
            None => ()
        }
    }

    /// Get block reference by path
    pub fn block(
        &mut self,
        path: PathBuf,
        open_options: MmapOpenOptions,
        pinned: bool
    ) -> Result<WeakBlock> {
        let mem_block = self.blocks_map.get(&path).map(Rc::clone);
        //Block found in memory
        if mem_block.is_some() {
            let mem_block = mem_block.unwrap();

            //Refresh block
            self.refresh_block(&mem_block);
            //Return block
            return Ok(Rc::downgrade(&mem_block));
        }

        //Open memory-mapped buffer
        let buffer = MmapBuffer::open(self.root_dir.join(path.clone()), open_options)?;
        //Remove blocks until there is enough space for new block
        let block_size = block_size(&buffer);
        while self.max_size-self.current_size<block_size {
            self.remove_lru_block();
        }
        //Create and insert block
        let new_block = self.add_block(path, buffer, pinned);

        Ok(Rc::downgrade(&new_block))
    }
}
