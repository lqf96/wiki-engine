# 海量数据检索系统与即时检索系统报告
鲁其璠 2014013423 lqf14@mails.tsinghua.edu.cn  
古志诚 2014013442 guzc14@mails.tsinghua.edu.cn  
刘世宸 2014013451 liusc14@mails.tsinghua.edu.cn  

## 环境配置
### 安装Rust
Rust是一种系统编程语言。它有着惊人的运行速度，能够防止段错误，并保证线程安全。本工程中索引建立与查询程序（同时也是后端服务器）均以Rust写成。

在macOS上，通过Homebrew安装Rust：

```sh
brew install rust
```

在Linux上，分别安装Rust编译器和Rust包管理器Cargo。以Debian系操作系统为例：

```sh
sudo apt-get install rustc cargo
```

### 安装Python库
本工程中维基百科数据集的爬取程序和预处理使用Python完成，需要安装相关的Python库才能运行。注意相关程序只能在Python 3.5以上版本下运行。

建立并激活Python虚拟环境（可选）：

```sh
virtualenv -p python3 venv
. venv/bin/activate
```

安装所需的库（Beautifulsoup 4和Requests）：

```sh
pip install requests beautifulsoup4
```

### 编译索引建立和查询程序
切换到工程目录下，使用Cargo编译索引建立和查询程序：

```sh
cargo build --release
```

编译时会首先下载和编译依赖，然后编译工程本身。

## 数据集下载和预处理
### 下载维基百科数据集
维基百科官方并未直接提供离线版HTML格式维基百科的下载，因此我们需要由其它途径下载数据集。

#### 使用爬虫下载数据集
在`tools`文件夹中提供了`crawler.py`用于爬取维基百科数据集，它的使用方法如下：

```
crawler.py [-h] -a ARTICLE -o OUTPUT_DIR [-n MAX_LENGTH]
           [-p URL_PREFIX]
```

其中各参数的含义如下：

* `-a`/`--article`：开始爬取的文章的名称。
* `-o`/`--output-dir`：存放爬取结果的文件夹。
* `-n`/`--max-length`：最多爬取的文章篇数。
* `-p`/`--url-prefix`：维基百科文章词条的网址前缀，默认是`https://en.wikipedia.org/wiki/`。

需要注意的是，官方不建议使用爬虫从维基百科爬取页面，所以请不要采用该方法实际爬取大量页面。

#### 从Kiwix下载数据集
[Kiwix](http://www.kiwix.org/)提供了离线版英文维基百科的下载，相关的种子文件可以在[此处](http://download.kiwix.org/zim/wikipedia_en_all_nopic.zim.torrent)下载。

从Kiwix下载得到的文档集是ZIM格式，这是Kiwix为储存维基百科专门设计的一种高度压缩的文档集格式，需要使用工具进行解压，这里我们使用[dignifiedquire/zim](https://github.com/dignifiedquire/zim)解压。

首先从Github上下载该程序源码，然后使用Cargo编译该程序，方法同样是切换至工程目录然后执行`cargo build --release`。之后执行`cargo run -- -o <Original Dataset Dir> <ZIM File>`解压维基百科文档集到指定文件夹中。在本机上实际操作时，解压位置是`/Volumes/Wikipedia/dataset`。

建议将数据集解压到单独分卷中以便稍后能够更加容易地清理相关数据，并且避免耗尽inode。

### 预处理数据集
下载得到的维基百科数据集需要进行预处理才能够进行索引建立。首先我们使用`tools`文件夹中的`slice_dataset.py`切分出一定数量的词条列表，该工具的使用方法如下：

```
slice_dataset.py [-h] -i INPUT_DIR -o OUTPUT_DIR -s SLICE_SIZE
                 [-n NUM_SLICES] [-p SLICE_NAME_PREFIX]
                 [-m {symlink,hardlink,clone,copy}] [-d]
```

各参数的含义如下所示：

* `-i`/`--input-dir`：下载的维基百科数据集的文章文件集，对应于上一步中的`<Original Dataset Dir>/A`。
* `-o`/`--output-dir`：切分出的子数据集或词条列表所在的文件夹。
* `-s`/`--slice-size`：每一个子数据集或者词条列表中包含多少篇文章。
* `-n`/`--num-slices`：子数据集或者词条列表的个数。
* `-p`/`--slice-name-prefix`：子数据集或者词条列表文件（夹）名称的前缀。
* `-m`/`--copy-method`：数据集HTML文件的复制方式，可选方式包括硬链接、软链接、写时复制和复制四种，默认是复制。
* `-d`/`--dry-run`：只生成词条列表，不拷贝词条文件和生成子数据集。

在本机上实际操作时，运行`./slice_dataset.py -i /Volumes/Wikipedia/dataset/A -o /Volumes/Wikipedia/sliced_dataset -s 40000 -n 24 -d`，即只生成24个词条列表，每个列表中有40000个词条的网页文件名称，不拷贝和生成子数据集。

然后，使用`tools`文件夹下的`preprocess.py`对数据集进行预处理，该工具的使用方法如下所示：

```
preprocess.py [-h] -i DATASET_PATH -o RESULT_PATH -g GRAM_PATH -f FILES
              [FILES ...] [-s STOP_WORDS]
```

各参数的含义如下所示：

* `-i`/`--dataset-path`：HTML数据集文件夹路径。
* `-o`/`--result-path`：预处理数据集结果所在的文件夹，预处理过程将每个词条对应的网页转换为一个文本文件，文件中包含了文章中出现的全部词与相应词频。
* `-g`/`--gram-path`：预处理数据集得到的`N-Gram`结果所在的文件夹，每一个词条的`N-Gram`结果包含了该文章某个词与该词最可能紧跟的下一个词的列表。
* `-f`/`--files`：需要处理的词条的列表，多个词条列表会进行并行处理。
* `-s`/`--stop-words`：英文停用词表，默认是同目录下的`stop_words.txt`。

在本机上实际操作时，运行`./preprocess.py -i /Volumes/Wikipedia/dataset/A -o /Volumes/Wikipedia/result -g /Volumes/Wikipedia/gram -f /Volumes/Wikipedia/sliced_dataset/slice0.txt ... /Volumes/Wikipedia/sliced_dataset/slice23.txt`对数据集进行预处理。

## 构建索引和运行引擎
### 构建维基百科索引
切换到工程的`wibuild`目录下，通过`cargo run`运行索引构建工具，其用法如下：

```
wibuild -i <INPUT_DIR> -o <INDEX_DIR>
```

其中`INPUT_DIR`是上一步预处理后得到的结果文件夹，`INDEX_DIR`是工具建立的索引所在的文件夹。实际运行过程的指令是`cargo run --release -- -i /Volumes/Wikipedia/result -o /Volumes/Wikipedia/index`。

建立好的索引保存在给定文件夹中，其下有`article_header`、`word_header`和`matrix`三个文件夹，分别存放文章信息、词信息和文章-词稀疏矩阵（十字链表）。每个文件夹中的一系列`block`文件存放数据，`info.json`则用来描述存放数据的信息和分块个数等。

### 运行引擎
切换到工程的`wiserve`目录下，通过`cargo run`运行引擎，其用法如下：

```
wiserve -i <INDEX_DIR> -s <STATIC_DIR> [-h <HOST>] [-p <PORT>]
```

其中`INDEX_DIR`是上一步中建立的索引所在的文件夹，`STATIC_DIR`则是引擎运行时前端静态文件所在的根目录，`HOST`和`PORT`指定了引擎运行过程中HTTP服务器所在的地址和端口，默认是`localhost:8080`。实际运行过程的指令是`cargo run --release -- -i /Volumes/Wikipedia/index -s ../frontend`。

当引擎开始运行后，打开`http://localhost:8080/search_main.html`，然后在搜索框中输入文字，即可开始搜索。输入文字时，网页会对输入进行提示、纠错和补全。搜索结果出现时，会按被搜索词出现的频度排序，并且被搜索词会高亮显示，点击搜索词链接则会到达相应维基百科的页面。
