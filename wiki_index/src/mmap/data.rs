use std::ops::{Deref, DerefMut};
use std::marker::{PhantomData};
use std::mem::{size_of};

use error::{Result};
use mmap::buffer::{RcMmapBuffer};

/// Memory-mapped data wrapper
#[derive(Clone, Debug)]
pub struct MmapData<T> {
    /// Data offset
    pub(super) offset: u32,
    /// Mmap buffer reference
    buffer: RcMmapBuffer,
    /// Type marker
    marker: PhantomData<T>
}

/// Immutable reference to data wrapper
#[derive(Debug)]
pub struct MmapDataRef<'w, T: 'w>(&'w MmapData<T>);

/// Mutable reference to data wrapper
#[derive(Debug)]
pub struct MmapDataMut<'w, T: 'w>(&'w MmapData<T>);

impl<T> MmapData<T> {
    /// Create new memory-mapped data wrapper
    pub(super) unsafe fn new(buffer: RcMmapBuffer, offset: u32) -> Self {
        MmapData {
            buffer: buffer,
            offset: offset,
            marker: PhantomData
        }
    }

    /// Allocate memory and create data wrapper
    pub fn alloc(buffer: RcMmapBuffer) -> Result<Self> {
        //Allocated memory and get offset
        let alloc_offset = buffer.borrow_mut().alloc_mem(size_of::<T>() as u32)?;

        unsafe { Ok(MmapData::new(buffer, alloc_offset)) }
    }

    /// Immutably borrow wrapped data
    pub fn borrow<'w>(&'w self) -> MmapDataRef<'w, T> {
        //Add an immutable borrow to borrow state
        self.buffer.borrow().add_borrow();

        MmapDataRef(&self)
    }

    /// Mutably borrow wrapped data
    pub fn borrow_mut<'w>(&'w self) -> MmapDataMut<'w, T> {
        //Add an immutable borrow to borrow state
        self.buffer.borrow().add_mut_borrow();

        MmapDataMut(&self)
    }
}

impl<'w, T> Drop for MmapDataRef<'w, T> {
    /// Destruct reference to data wrapper
    fn drop(&mut self) {
        //Remove an immutable borrow from borrow state
        self.0.buffer.borrow().remove_borrow();
    }
}

impl<'w, T> Drop for MmapDataMut<'w, T> {
    /// Destruct mutable reference to data wrapper
    fn drop(&mut self) {
        //Remove an mutable borrow from borrow state
        self.0.buffer.borrow().remove_mut_borrow();
    }
}

impl<'w, T> Deref for MmapDataRef<'w, T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { &*self.0.buffer.borrow().pointer::<T>(self.0.offset) }
    }
}

impl<'w, T> Deref for MmapDataMut<'w, T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { &*self.0.buffer.borrow().pointer::<T>(self.0.offset) }
    }
}

impl<'w, T> DerefMut for MmapDataMut<'w, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.0.buffer.borrow_mut().pointer_mut::<T>(self.0.offset) }
    }
}
