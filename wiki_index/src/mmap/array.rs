use std::ops::{Deref, DerefMut};
use std::marker::{PhantomData};
use std::slice::{from_raw_parts, from_raw_parts_mut};
use std::mem::{size_of};

use error::{Result, Error, ErrorKind};
use mmap::buffer::{RcMmapBuffer};

/// Memory-mapped array wrapper
#[derive(Clone)]
pub struct MmapArray<T> {
    /// Data offset
    pub(super) offset: u32,
    /// Mmap buffer reference
    buffer: RcMmapBuffer,
    /// Type marker
    marker: PhantomData<T>
}

/// Immutable reference to data wrapper
pub struct MmapArrayRef<'w, T: 'w>(&'w MmapArray<T>);

/// Mutable reference to data wrapper
pub struct MmapArrayMut<'w, T: 'w>(&'w MmapArray<T>);

impl<T> MmapArray<T> {
    /// Create new memory-mapped array wrapper
    pub(super) unsafe fn new(buffer: RcMmapBuffer, offset: u32) -> Self {
        MmapArray {
            buffer: buffer,
            offset: offset,
            marker: PhantomData
        }
    }

    /// Automatically allocate array memory by reading array size
    pub fn alloc_auto(buffer: RcMmapBuffer) -> Result<Self> {
        let alloc_offset = {
            let mut buffer_mut = buffer.borrow_mut();
            //Allocate memory only for array size and read it first
            let alloc_offset = buffer_mut.alloc_mem(4)?;
            let size = unsafe { *buffer_mut.pointer::<u32>(alloc_offset) };

            //Allocate array data memory
            buffer_mut.alloc_mem(size*(size_of::<T>() as u32))?;

            alloc_offset
        };

        unsafe { Ok(MmapArray::new(buffer, alloc_offset)) }
    }

    /// Allocate memory and create array wrapper
    pub fn alloc(buffer: RcMmapBuffer, size: u32) -> Result<Self> {
        let array_size = size*(size_of::<T>() as u32)+4;
        //Allocated memory and get offset
        let alloc_offset = buffer.borrow_mut().alloc_mem(array_size)?;

        //Set size
        unsafe {
            let mut buffer_mut = buffer.borrow_mut();
            let size_ptr = buffer_mut.pointer_mut::<u32>(alloc_offset) as *mut u32;

            *size_ptr = size;
        }

        unsafe { Ok(MmapArray::new(buffer, alloc_offset)) }
    }

    /// Immutably borrow wrapped data
    pub fn borrow<'w>(&'w self) -> MmapArrayRef<'w, T> {
        //Add an immutable borrow to borrow state
        self.buffer.borrow().add_borrow();

        MmapArrayRef(&self)
    }

    /// Mutably borrow wrapped data
    pub fn borrow_mut<'w>(&'w self) -> MmapArrayMut<'w, T> {
        //Add an immutable borrow to borrow state
        self.buffer.borrow().add_mut_borrow();

        MmapArrayMut(&self)
    }

    /// Push an item into the array
    pub fn push(&mut self, value: T) -> Result<()> {
        let mut buffer_mut = self.buffer.borrow_mut();
        //Underlying buffer must be writable
        if !buffer_mut.options.writable {
            return Err(Error::simple(ErrorKind::NonWritable));
        }

        let size_ptr_mut = buffer_mut.pointer_mut::<u32>(self.offset);
        //Array size and type size
        let array_size = unsafe { *size_ptr_mut };
        let type_size = size_of::<T>() as u32;
        //Array is still at the end of the buffer
        if self.offset+4+array_size*type_size!=buffer_mut.pos {
            return Err(Error::new(
                ErrorKind::NonExpandable,
                "Array must be at the end of the buffer in order to expand"
            ));
        }

        //Allocate memory for new item
        let value_offset = buffer_mut.alloc_mem(type_size)?;
        let value_ptr = buffer_mut.pointer_mut::<T>(value_offset);
        //Set item and update array size
        unsafe {
            *value_ptr = value;
            *size_ptr_mut += 1;
        }

        Ok(())
    }
}

impl<'w, T> Drop for MmapArrayRef<'w, T> {
    /// Destruct reference to data wrapper
    fn drop(&mut self) {
        //Remove an immutable borrow from borrow state
        self.0.buffer.borrow().remove_borrow();
    }
}

impl<'w, T> Drop for MmapArrayMut<'w, T> {
    /// Destruct mutable reference to data wrapper
    fn drop(&mut self) {
        //Remove an mutable borrow from borrow state
        self.0.buffer.borrow().remove_mut_borrow();
    }
}

impl<'w, T> Deref for MmapArrayRef<'w, T> {
    type Target = [T];

    fn deref(&self) -> &[T] {
        let payload_ptr = self.0.buffer.borrow().pointer::<u8>(self.0.offset);
        let size_ptr = payload_ptr as *const u32;
        let data_ptr = payload_ptr.wrapping_offset(4) as *const T;

        unsafe { from_raw_parts(data_ptr, *size_ptr as usize) }
    }
}

impl<'w, T> Deref for MmapArrayMut<'w, T> {
    type Target = [T];

    fn deref(&self) -> &[T] {
        let payload_ptr = self.0.buffer.borrow().pointer::<u8>(self.0.offset);
        let size_ptr = payload_ptr as *const u32;
        let data_ptr = payload_ptr.wrapping_offset(4) as *const T;

        unsafe { from_raw_parts(data_ptr, *size_ptr as usize) }
    }
}

impl<'w, T> DerefMut for MmapArrayMut<'w, T> {
    fn deref_mut(&mut self) -> &mut [T] {
        let payload_ptr_mut = self.0.buffer.borrow_mut().pointer_mut::<u8>(self.0.offset);
        let size_ptr = payload_ptr_mut as *const u32;
        let data_ptr_mut = payload_ptr_mut.wrapping_offset(4) as *mut T;

        unsafe { from_raw_parts_mut(data_ptr_mut, *size_ptr as usize) }
    }
}
