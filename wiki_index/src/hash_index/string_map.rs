use std::collections::hash_map::{DefaultHasher};
use std::hash::{Hash, Hasher};
use std::cell::{RefCell};
use std::rc::{Rc};

use error::{Result, Error, ErrorKind};
use mmap::{MmapOpenOptions, MmapData, MmapArray};
use collection::{CollectionType, CollectionBase, CollectionLink};
use block::{RcBlockManager};
use util::{get_bits, msb_index};
use hash_index::common::{MetadataInfo, MetadataItem};
use hash_index::sparse_matrix::{MatrixItem};

/// Map entry
#[derive(Clone, Debug)]
pub struct MapEntry {
    /// Key
    pub key: CollectionLink<u8>,
    /// Hash value
    pub(super) hash: u64,
    /// Next map entry
    next: CollectionLink<MapEntry>,
    /// Metadata item linked list
    metadata: CollectionLink<MetadataItem>,
    /// Head of matrix linked list
    pub list_head: CollectionLink<MatrixItem>,
}

/// Hash index entry
#[derive(Clone, Debug)]
enum HashEntry {
    /// No data
    Nothing,
    /// Child hash table
    ChildTable(CollectionLink<HashJumpTable>),
    /// Items list
    ItemsList(CollectionLink<MapEntry>),
}

/// Level-wise hash index jump table
#[derive(Clone, Debug)]
struct HashJumpTable {
    /// Prefix size
    prefix_size: u8,
    /// Number of elements
    n_elements: u32,
    /// Leaf table
    leaf: bool,
    /// Jump table
    jump_table: [CollectionLink<HashEntry>; 16]
}

/// Jump table insert item
#[derive(Clone, Debug)]
enum InsertItemType {
    /// New entry
    NewEntry,
    /// Existing entry
    ExistingEntry(CollectionLink<MapEntry>)
}

/// Block-partitioned string map
#[derive(Debug)]
pub struct StringMap {
    /// Collection base
    pub base: CollectionBase,
    /// Maximum prefix size
    prefix_size: u8,
    /// Metadata information linked list
    metadata_info: MmapData<CollectionLink<MetadataInfo>>,
    /// Root-level hash index jump table
    root_table: MmapData<HashJumpTable>
}

/// Reference-counted string map
pub type RcStringMap = Rc<RefCell<StringMap>>;

/// Get jump table index and entries index
fn lookup_table_index(table: MmapData<HashJumpTable>, hash: u64, prefix_size: u8) -> (usize, usize) {
    //Get part of the hash value
    let new_prefix_size = prefix_size+table.borrow().prefix_size;
    let hash_part = get_bits(hash, prefix_size, new_prefix_size);
    //Get jump table index
    let mut jump_table_index = msb_index(hash_part);
    if jump_table_index!=0 {
        jump_table_index -= 1;
    }
    //Get entries table index
    let entries_index = if jump_table_index==0 {
        hash_part
    } else {
        hash_part-(1<<jump_table_index)
    };

    (jump_table_index as usize, entries_index as usize)
}

impl MapEntry {
    /// Create a new map entry
    pub fn new() -> MapEntry {
        MapEntry {
            key: CollectionLink::empty(),
            hash: 0,
            next: CollectionLink::empty(),
            metadata: CollectionLink::empty(),
            list_head: CollectionLink::empty()
        }
    }
}

impl HashJumpTable {
    /// Create a new hash jump table
    fn new(base: &mut CollectionBase) -> Result<HashJumpTable> {
        //Initial entries
        let init_entries_link = base.write_vary(
            &[HashEntry::Nothing, HashEntry::Nothing]
        )?;

        //Jump table
        let empty_link = CollectionLink::empty();
        let jump_table_links = [
            init_entries_link, empty_link.clone(), empty_link.clone(), empty_link.clone(),
            empty_link.clone(), empty_link.clone(), empty_link.clone(), empty_link.clone(),
            empty_link.clone(), empty_link.clone(), empty_link.clone(), empty_link.clone(),
            empty_link.clone(), empty_link.clone(), empty_link.clone(), empty_link.clone()
        ];

        //Jump table object
        Ok(HashJumpTable {
            prefix_size: 1,
            n_elements: 0,
            leaf: true,
            jump_table: jump_table_links
        })
    }
}

impl StringMap {
    /// Create a new index collection
    pub fn new(
        block_manager: RcBlockManager,
        name: String,
        collection_type: CollectionType,
        open_options: MmapOpenOptions
    ) -> Result<StringMap> {
        //Collection base
        let mut base = CollectionBase::new(
            block_manager,
            name,
            collection_type,
            open_options.clone()
        )?;

        //Pin first block in memory
        let buffer_0 = base.block_by_id(0, true)?.buffer.clone();
        //Metadata information and root table
        let metadata_info = MmapData::alloc(buffer_0.clone())?;
        let root_table = MmapData::alloc(buffer_0.clone())?;

        //Initialize data structure
        if open_options.create {
            //Initialize root jump table
            *root_table.borrow_mut() = HashJumpTable::new(&mut base)?;
        }

        Ok(StringMap {
            base: base,
            prefix_size: 0,
            metadata_info: metadata_info,
            root_table: root_table
        })
    }

    /// Get map entry by key
    pub fn get(&mut self, key: &String) -> Result<CollectionLink<MapEntry>> {
        //Calculate hash value of the key
        let mut hasher = DefaultHasher::new();
        key.hash(&mut hasher);
        let key_hash = hasher.finish();

        //Get items linked list head
        let root_table = self.root_table.clone();
        //Recursive get through jump table
        self.table_get(root_table, key_hash, 0)
    }

    /// Insert or update key-value pair into map
    pub fn insert(&mut self, key: String) -> Result<CollectionLink<MapEntry>> {
        //Calculate hash value of the key
        let mut hasher = DefaultHasher::new();
        key.hash(&mut hasher);
        let key_hash = hasher.finish();

        let root_table = self.root_table.clone();
        //Insert to root table
        self.table_insert(root_table, key_hash, 0, key, InsertItemType::NewEntry)
    }

    /// Get entry reference
    fn get_entry_index(
        &mut self,
        table: MmapData<HashJumpTable>,
        hash: u64,
        prefix_size: u8
    ) -> Result<(MmapArray<HashEntry>, usize)> {
        //Jump table index and entries table index
        let (jump_table_index, entries_index) = lookup_table_index(table.clone(), hash, prefix_size);
        //Get jump table link
        let jump_table_link = table.borrow().jump_table[jump_table_index].clone();
        let entries = jump_table_link.try_get_array(&mut self.base)?;

        Ok((entries, entries_index))
    }

    /// Get hash entry on level-wise table by hash
    fn table_get(
        &mut self,
        table: MmapData<HashJumpTable>,
        hash: u64,
        prefix_size: u8
    ) -> Result<CollectionLink<MapEntry>> {
        let new_prefix_size = prefix_size+table.borrow().prefix_size;

        let (entries, entries_index) = self.get_entry_index(table, hash, prefix_size)?;
        //Get entry
        let entry = entries.borrow()[entries_index].clone();

        match entry {
            //Not found
            HashEntry::Nothing => Err(Error::simple(ErrorKind::NotFound)),
            //Find in child table
            HashEntry::ChildTable(ref child_table_link) => {
                let child_table = child_table_link.try_get_data(&mut self.base)?;

                self.table_get(child_table, hash, new_prefix_size)
            },
            //Find in items list
            HashEntry::ItemsList(ref item_link) => {
                let mut item_link = item_link.clone();

                loop {
                    let item = item_link.try_get_data(&mut self.base)?;
                    //Found
                    if item.borrow().hash==hash {
                        return Ok(item_link);
                    }

                    //Move to next link
                    item_link = item.borrow().next.clone();
                    if item_link.is_empty() {
                        return Err(Error::simple(ErrorKind::NotFound));
                    }
                }
            }
        }
    }

    /// Insert or update key-value
    fn table_insert(
        &mut self,
        table: MmapData<HashJumpTable>,
        hash: u64,
        prefix_size: u8,
        key: String,
        insert_entry: InsertItemType
    ) -> Result<CollectionLink<MapEntry>> {
        //Prefix size
        let (table_prefix_size, n_elements, mut leaf) = {
            let table_ref = table.borrow();

            (table_ref.prefix_size, table_ref.n_elements, table_ref.leaf)
        };
        //Expand hash table
        if n_elements==(1<<(table_prefix_size-1)) {
            if table_prefix_size<10 {
                self.table_expand(table.clone())?;
            } else {
                {
                    let mut table_mut = table.borrow_mut();
                    //Current table is no longer leaf node
                    table_mut.leaf = false;
                    table_mut.n_elements += 1;
                }
                leaf = false;

                //Create child tables
                self.table_expand_create_child(table.clone(), prefix_size)?;
            }
        }

        let new_prefix_size = prefix_size+table.borrow().prefix_size;
        //Get entries and index
        let (entries, entries_index) = self.get_entry_index(table.clone(), hash, prefix_size)?;
        //Get entry
        let entry = entries.borrow()[entries_index].clone();

        match entry {
            //Insert element at the position
            HashEntry::Nothing => {
                let map_entry_link = match insert_entry {
                    //New entry
                    InsertItemType::NewEntry => {
                        //Update number of elements
                        if leaf {
                            table.borrow_mut().n_elements += 1;
                        }

                        //Key link
                        let key_link = self.base.write_vary(key.as_bytes())?;
                        //Allocate space for new entry
                        let (block_id, new_entry) = self.base.alloc_data::<MapEntry>()?;
                        let mut new_entry_mut = new_entry.borrow_mut();

                        //Copy string to memory
                        new_entry_mut.key = key_link;
                        //Set hash
                        new_entry_mut.hash = hash;

                        CollectionLink::new(block_id, new_entry.as_link())
                    },
                    //Existing item
                    InsertItemType::ExistingEntry(ref entry_link) => {
                        entry_link.clone()
                    }
                };

                //Create child table if current table is full
                let hash_entry_link = if !leaf {
                    //Create a new child table
                    let (block_id, child_table) = self.base.alloc_data()?;
                    *child_table.borrow_mut() = HashJumpTable::new(&mut self.base)?;

                    //Insert into child table
                    self.table_insert(
                        child_table.clone(),
                        hash,
                        new_prefix_size,
                        key,
                        InsertItemType::ExistingEntry(map_entry_link.clone())
                    )?;

                    //Child table
                    HashEntry::ChildTable(
                        CollectionLink::new(block_id, child_table.as_link())
                    )
                } else {
                    //Items list
                    HashEntry::ItemsList(map_entry_link.clone())
                };

                //Set hash link
                entries.borrow_mut()[entries_index] = hash_entry_link;

                Ok(map_entry_link)
            }
            //Insert in child table
            HashEntry::ChildTable(ref child_table_link) => {
                let child_table = child_table_link.try_get_data(&mut self.base)?;

                self.table_insert(child_table, hash, new_prefix_size, key, insert_entry)
            },
            //Find in items list
            HashEntry::ItemsList(ref item_link) => {
                let mut item = item_link.try_get_data(&mut self.base)?;

                //Traverse through linked list
                loop {
                    //Key matched
                    match insert_entry {
                        InsertItemType::NewEntry => {
                            let item_hash = item.borrow().hash;

                            if item_hash==hash {
                                let item_key_link = item.borrow().key.clone();
                                let item_key = item_key_link.try_get_array(&mut self.base)?;

                                if &*item_key.borrow()==key.as_bytes() {
                                    return Ok(item_link.clone());
                                }
                            }
                        }
                        InsertItemType::ExistingEntry(..) => ()
                    }

                    //Move to next item
                    let next_item_link = item.borrow().next.clone();
                    if next_item_link.is_empty() {
                        break;
                    } else {
                        item = next_item_link.try_get_data(&mut self.base)?;
                    }
                }

                let entry_link = match insert_entry {
                    //New entry
                    InsertItemType::NewEntry => {
                        //Update number of elements
                        if leaf {
                            table.borrow_mut().n_elements += 1;
                        }

                        //Key link
                        let key_link = self.base.write_vary(key.as_bytes())?;
                        //Allocate space for new entry
                        let (block_id, new_entry) = self.base.alloc_data::<MapEntry>()?;
                        let mut new_entry_mut = new_entry.borrow_mut();

                        //Copy string to memory
                        new_entry_mut.key = key_link;
                        //Set hash
                        new_entry_mut.hash = hash;

                        CollectionLink::new(block_id, new_entry.as_link())
                    },
                    //Existing item
                    InsertItemType::ExistingEntry(ref entry_link) => {
                        entry_link.clone()
                    }
                };

                //Set next item
                item.borrow_mut().next = entry_link.clone();

                Ok(entry_link)
            }
        }
    }

    /// Expand table
    fn table_expand(
        &mut self,
        table: MmapData<HashJumpTable>
    ) -> Result<()> {
        let prefix_size = table.borrow().prefix_size;
        //Allocate entries for table
        let (block_id, new_entries) = self.base.alloc_vary::<HashEntry>(1<<prefix_size)?;
        for entry in new_entries.borrow_mut().iter_mut() {
            *entry = HashEntry::Nothing;
        }
        //Create link for new entries
        let new_entries_link = CollectionLink::new(block_id, new_entries.as_link());

        {
            let mut table_mut = table.borrow_mut();
            //Set new entries
            table_mut.jump_table[prefix_size as usize] = new_entries_link;
            //Update prefix size
            table_mut.prefix_size += 1;
        }

        let mut entries_index = prefix_size;
        //Rearrange entries
        while entries_index!=0 {
            let new_entries_link = table.borrow().jump_table[entries_index as usize].clone();
            let old_entries_link = table.borrow().jump_table[(entries_index-1) as usize].clone();
            //Get new and old entries
            let new_entries = new_entries_link.try_get_array(&mut self.base)?;
            let old_entries = old_entries_link.try_get_array(&mut self.base)?;

            let mut i = old_entries.borrow().len()-1;
            loop {
                //Get entry
                let entry = old_entries.borrow()[i].clone();

                match entry {
                    HashEntry::Nothing => (),
                    HashEntry::ChildTable(..) => panic!("Child table should not appear here!"),
                    HashEntry::ItemsList(ref item_link) => {
                        let mut item_link = item_link.clone();
                        let mut item = item_link.try_get_data(&mut self.base)?;
                        //Zero and one linked list head
                        let mut zero_head = CollectionLink::empty();
                        let mut one_head = CollectionLink::empty();
                        //Zero and one linked list tail
                        let mut zero_tail = CollectionLink::empty();
                        let mut one_tail = CollectionLink::empty();

                        //Rearrange items
                        loop {
                            let hash = item.borrow().hash;
                            //Get bit
                            let bit = get_bits(hash, prefix_size, prefix_size+1);

                            //Current tail
                            let (current_head, current_tail) = if bit==1 {
                                (&mut one_head, &mut one_tail)
                            } else {
                                (&mut zero_head, &mut zero_tail)
                            };
                            //Update tail
                            *current_tail = if current_tail.is_empty() {
                                //Set linked list head
                                *current_head = item_link.clone();

                                item_link
                            } else {
                                let current_item = current_tail.try_get_data(&mut self.base)?;
                                //Set next link
                                current_item.borrow_mut().next = item_link.clone();

                                item_link
                            };

                            //Move to next item
                            item_link = item.borrow().next.clone();
                            if item_link.is_empty() {
                                break;
                            } else {
                                let next_item = item_link.try_get_data(&mut self.base)?;
                                //Reset next item
                                item.borrow_mut().next = CollectionLink::empty();
                                item = next_item;
                            }
                        }

                        //Set entries
                        {
                            let (mut entries_mut, index) = {
                                let entries_mut = if entries_index==1 && i==0 {
                                    old_entries.borrow_mut()
                                } else {
                                    new_entries.borrow_mut()
                                };
                                let index = if entries_index>1 { 2*i } else { 0 };

                                (entries_mut, index)
                            };

                            //Update entries
                            entries_mut[index] = if zero_head.is_empty() {
                                HashEntry::Nothing
                            } else {
                                HashEntry::ItemsList(zero_head)
                            };
                            entries_mut[index+1] = if one_head.is_empty() {
                                HashEntry::Nothing
                            } else {
                                HashEntry::ItemsList(one_head)
                            };
                        }
                    }
                }

                if i==0 {
                    break;
                } else {
                    i -= 1;
                }
            }

            entries_index -= 1;
        }

        Ok(())
    }

    /// Expand table and create child
    fn table_expand_create_child(
        &mut self,
        table: MmapData<HashJumpTable>,
        prefix_size: u8
    ) -> Result<()> {
        //Traverse through jump table and entry table
        for i in 0..10 {
            //Get entries
            let entries_link = table.borrow().jump_table[i].clone();
            let entries = entries_link.try_get_array(&mut self.base)?;

            println!("Redistribute items to child table for entries part #{}", i);

            let entries_len = entries.borrow().len();
            for j in 0..entries_len {
                //Get hash entry
                let hash_entry = entries.borrow()[j].clone();

                match hash_entry {
                    HashEntry::Nothing => (),
                    HashEntry::ChildTable(..) => panic!("Child table should not appear here!"),
                    HashEntry::ItemsList(ref item_link) => {
                        let (block_id, child_table) = self.base.alloc_data()?;
                        *child_table.borrow_mut() = HashJumpTable::new(&mut self.base)?;

                        //Current item
                        let mut item_link = item_link.clone();
                        //Insert items into child table
                        while !item_link.is_empty() {
                            //Get item and hash value
                            let item = item_link.try_get_data(&mut self.base)?;
                            let hash = item.borrow().hash;
                            //Get and clear next item
                            let next_item_link = item.borrow().next.clone();
                            item.borrow_mut().next = CollectionLink::empty();

                            self.table_insert(
                                child_table.clone(),
                                hash,
                                prefix_size+10,
                                String::new(),
                                InsertItemType::ExistingEntry(item_link.clone())
                            )?;

                            //Move to next item
                            item_link = next_item_link;
                        }

                        //Update child table link
                        entries.borrow_mut()[j] = HashEntry::ChildTable(
                            CollectionLink::new(block_id, child_table.as_link())
                        );
                    }
                }
            }
        }

        Ok(())
    }
}
