pub mod collection;
pub mod link;

pub use self::collection::{CollectionType, CollectionBase};
pub use self::link::{CollectionLink};
