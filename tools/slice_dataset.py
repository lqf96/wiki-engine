#! /usr/bin/env python3
import os, shutil, platform, subprocess
from argparse import ArgumentParser

def clone_file(src, dst):
    # macOS
    if OS_NAME=="Darwin":
        cp_args = ["cp", "-c", src, dst]
    elif OS_NAME=="Linux":
        cp_args = ["cp", "--reflink=always", src, dst]
    else:
        raise NotImplemented("Clone not implemented for other platform")
    # Perform clone
    subprocess.run(cp_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)

# OS name
OS_NAME = platform.system()
# Copy functions
COPY_FUNCS = {
    "symlink": os.symlink,
    "hardlink": os.link,
    "clone": clone_file,
    "copy": shutil.copy
}

def main():
    parser = ArgumentParser()
    # Arguments
    parser.add_argument(
        "-i", "--input-dir",
        help="Input Wikipedia HTML dataset directory",
        required=True
    )
    parser.add_argument(
        "-o", "--output-dir",
        help="Output sliced dataset directory",
        required=True
    )
    parser.add_argument(
        "-s", "--slice-size",
        help="Dataset slice size",
        type=int,
        required=True
    )
    parser.add_argument(
        "-n", "--num-slices",
        help="Number of slices",
        type=int,
        default=1
    )
    parser.add_argument(
        "-p", "--slice-name-prefix",
        help="Dataset slice name prefix",
        default="slice"
    )
    parser.add_argument(
        "-m", "--copy-method",
        help="File copy method",
        choices=["symlink", "hardlink", "clone", "copy"],
        default="copy"
    )
    parser.add_argument(
        "-d", "--dry-run",
        help="Generate lists only and do not copy file",
        action="store_true"
    )
    # Parse arguments
    args = parser.parse_args()
    # Create output directory
    os.makedirs(args.output_dir, exist_ok=True)
    # Get input directory iterator
    entry_iter = os.scandir(args.input_dir)
    # Copy function
    copy_func = COPY_FUNCS[args.copy_method]
    # Make slices
    for i in range(args.num_slices):
        print("[main] Copying slice %d" % i)
        slice_name = args.slice_name_prefix+str(i)
        # Create slice directory
        if not args.dry_run:
            os.makedirs(os.path.join(args.output_dir, slice_name), exist_ok=True)
        # Copy files
        with open(os.path.join(args.output_dir, slice_name+".txt"), "w") as f:
            for j in range(args.slice_size):
                # Get next entry
                entry = next(entry_iter)
                if j%1000==0:
                    print("[main] Copying file #%d '%s' in slice %d" % (j, entry.name, i))
                # Write file name
                f.write(entry.name+"\n")
                if not args.dry_run:
                    # Full source and destination path
                    full_src = os.path.join(args.input_dir, entry.name)
                    full_dst = os.path.join(args.output_dir, slice_name, entry.name)
                    # Copy file
                    copy_func(full_src, full_dst)

if __name__=="__main__":
    main()
